# DrupalSpoons

This composer plugin prepares your Drupal project for CI and/or local tests.

## Installation

```shell script
curl -s https://gitlab.com/drupalspoons/composer-plugin/-/raw/master/bin/setup | bash
```

## Usage

Use https://direnv.net/ or similar to configure your local environment. See the new .envrc file in your project root. If you don't want to install `direnv`, prefix all Drush and Composer commands like `COMPOSER=composer.spoons.json drush ...` .

- If the project's composer.json changes:
    ```
    composer drupalspoons:prepare
    ```
- If the project adds or removes files or directories in its root:
    ```
    composer drupalspoons:configure
    ```
- If new or updated dependencies are available:
    ```
    composer update
    ```
